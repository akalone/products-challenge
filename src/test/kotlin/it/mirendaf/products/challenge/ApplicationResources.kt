package it.mirendaf.products.challenge

import java.net.URL

object ApplicationResources {
    private const val BaseMockPath = "/mock"
    private const val JsonExt = "json"

    private fun URL.readTextWithTrims(): String {
        return this.readText().trimMargin().trimIndent()
    }

    object Products {
        val SuccessProducts = javaClass.getResource("$BaseMockPath/products_200.$JsonExt").readTextWithTrims()
    }

    object Mocks {
        val HelloWorld = javaClass.getResource("$BaseMockPath/hello-world.$JsonExt").readTextWithTrims()
    }
}