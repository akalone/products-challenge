package it.mirendaf.products.challenge.test

import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.netty.handler.codec.http.HttpResponseStatus.BAD_REQUEST
import io.netty.handler.codec.http.HttpResponseStatus.OK
import it.mirendaf.products.challenge.ApplicationResources
import it.mirendaf.products.challenge.Constants.ProductsFields
import it.mirendaf.products.challenge.products.GoRestProductsClient
import it.mirendaf.products.challenge.products.ProductsService
import org.junit.jupiter.api.Assertions.assertTrue
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.WebClient
import reactor.test.StepVerifier
import java.net.InetAddress


class GoRestProductsClientTest {

    private val categoryIdTest: Long = 8
    private val jsonResponseSample = ApplicationResources.Products.SuccessProducts
    private val json = jacksonObjectMapper().readTree(jsonResponseSample)
    private val dataSample = (json.get(ProductsFields.Data) as ArrayNode).toList()

    @Test
    @DisplayName("An error response from go rest products api should raise an error")
    fun testFailureGoRestProductsExchange() {

        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(BAD_REQUEST.code())
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .setBody(ApplicationResources.Mocks.HelloWorld)
        )

        val goRestApiCall = goRestProductsClient.getProducts { r ->
            val json = r.bodyToMono(String::class.java).map { s ->
                val jsonNode = jacksonObjectMapper().readTree(s)
                jsonNode.path(ProductsFields.Data) as ArrayNode
            }
            json
        }

        StepVerifier
            .create(goRestApiCall)
            .expectError()
            .verify()
    }

    @Test
    @DisplayName("A successful response from go rest products api should be correctly parsed")
    fun testSuccessGoRestProductsExchange() {

        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(OK.code())
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .setBody(ApplicationResources.Products.SuccessProducts)
        )

        val goRestApiCall = goRestProductsClient.getProducts { r ->
            val json = r.bodyToMono(String::class.java).map { s ->
                val jsonNode = jacksonObjectMapper().readTree(s)
                val dataJsonNode = jsonNode.path(ProductsFields.Data) as ArrayNode
                assertTrue(!dataJsonNode.isEmpty)

                val categoriesJsonNode = dataJsonNode.map { d -> d.path(ProductsFields.Categories) as ArrayNode}
                val isCategoriesFilled = categoriesJsonNode.all { !it.isEmpty}
                assertTrue(isCategoriesFilled)
            }
            json
        }

        StepVerifier
            .create(goRestApiCall)
            .expectNextCount(1)
            .expectComplete()
            .verify()
    }

    @Test
    @DisplayName("A list of data with categories should be filtered by given category id")
    fun filterByCategoryTest() {
        val filtered = ProductsService.filterByCategory(dataSample, categoryIdTest)
        val isCategoryIdAlwaysPresent = filtered.toList()
            .map{ it[ProductsFields.Categories] as ArrayNode}
            .map{ category -> category.toList()
                .map{ it[ProductsFields.CategoryId].asLong() }
            }.all{ c ->
                c.contains(categoryIdTest) }

        assertTrue(isCategoryIdAlwaysPresent)
    }

    companion object {
        private val mockWebServer: MockWebServer = MockWebServer()
        private lateinit var webClient:WebClient
        private lateinit var goRestProductsClient:GoRestProductsClient

        @BeforeAll
        @JvmStatic
        internal fun beforeAll() {
            mockWebServer.start(InetAddress.getByName("localhost"), 8080)
            webClient = WebClient.builder()
                .baseUrl(mockWebServer.url("/").toString()).build()
            goRestProductsClient = GoRestProductsClient(webClient)
        }

        @AfterAll
        @JvmStatic
        internal fun afterAll() {
            mockWebServer.close()
        }
    }

}