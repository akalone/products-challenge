package it.mirendaf.products.challenge.test

import io.netty.handler.codec.http.HttpResponseStatus.BAD_REQUEST
import io.netty.handler.codec.http.HttpResponseStatus.OK
import it.mirendaf.products.challenge.ApplicationResources
import it.mirendaf.products.challenge.products.GoRestProductsClient
import it.mirendaf.products.challenge.products.ProductsService
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.WebClient
import reactor.test.StepVerifier
import java.net.InetAddress


class ProductsServiceTest {

    @Test
    @DisplayName("Invalid response received from go rest api should raise an error")
    fun testFailureProductsService() {

        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(BAD_REQUEST.code())
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .setBody(ApplicationResources.Mocks.HelloWorld)
        )

        val getProductsCall = productsService.getProducts()

        StepVerifier
            .create(getProductsCall)
            .expectError()
            .verify()
    }

    @Test
    @DisplayName("Correct response received from go rest api should be correctly parsed")
    fun testSuccessProductsService() {

        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(OK.code())
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .setBody(ApplicationResources.Products.SuccessProducts)
        )

        val getProductsCall = productsService.getProducts()

        StepVerifier
            .create(getProductsCall)
            .expectNextCount(1)
            .expectComplete()
            .verify()
    }

    companion object {
        private val mockWebServer: MockWebServer = MockWebServer()
        private lateinit var webClient:WebClient
        private lateinit var goRestProductsClient:GoRestProductsClient
        private lateinit var productsService: ProductsService

        @BeforeAll
        @JvmStatic
        internal fun beforeAll() {
            mockWebServer.start(InetAddress.getByName("localhost"), 8080)
            webClient = WebClient.builder()
                .baseUrl(mockWebServer.url("/").toString()).build()
            goRestProductsClient = GoRestProductsClient(webClient)
            productsService = ProductsService(goRestProductsClient)
        }

        @AfterAll
        @JvmStatic
        internal fun afterAll() {
            mockWebServer.close()
        }
    }

}