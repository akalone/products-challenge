package it.mirendaf.products.challenge.test

import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.TextNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import it.mirendaf.products.challenge.ApplicationResources
import it.mirendaf.products.challenge.extensions.Extensions.extract
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ExtensionTest {

    @Test
    fun extractNodeByName() {
        val json = ApplicationResources.Mocks.HelloWorld
        val jsonNode = jacksonObjectMapper().readTree(json)
        val hello = jsonNode.extract<TextNode>("hello")
        val hellos = jsonNode.extract<ArrayNode>("hellos")

        assertEquals(TextNode.valueOf("world"), hello)
        assertEquals(2, hellos.size())

    }

}