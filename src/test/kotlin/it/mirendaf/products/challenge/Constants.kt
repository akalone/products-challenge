package it.mirendaf.products.challenge

object Constants {
    object ProductsFields {
        const val Data = "data"
        const val Categories = "categories"
        const val CategoryId = "id"
    }
}