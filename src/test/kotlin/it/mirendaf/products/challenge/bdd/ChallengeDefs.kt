package it.mirendaf.products.challenge.bdd

import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.cucumber.java8.En
import it.mirendaf.products.challenge.products.ProductsController
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import java.io.ByteArrayInputStream


@ExtendWith(SpringExtension::class)
@WebFluxTest(controllers = [ProductsController::class])
class ChallengeDefs(private val webClient: WebTestClient): En {
    private lateinit var uri: WebTestClient.RequestHeadersSpec<*>
    private lateinit var exchange: WebTestClient.ResponseSpec
    private lateinit var get: WebTestClient.RequestHeadersUriSpec<*>
    private val categoryIdTest: Long = 8

    init {
        Given("a GET call to /products") {
            uri = webClient.get()
                .uri("/products")
                .accept(MediaType.APPLICATION_JSON)
        }

        When("no additional parameter is passed") {
            exchange = uri.exchange()
        }

        Then("a list of products taken by https:\\/\\/gorest.co.in\\/public-api\\/products is returned") {
            exchange
                .expectStatus().isOk
                .expectBody()
                .jsonPath("data")
                .isArray
        }

        Given("a GET call to /products with no additional parameter") {
            get = webClient.get()

        }

        When("the categoryId Query Parameter is passed") {
            exchange = get.uri {uriBuilder ->
                uriBuilder.path("/products")
                    .queryParam("categoryId", "{categoryId}")
                    .build("$categoryIdTest")
            }.exchange()
        }

        Then("a list of products taken by https:\\/\\/gorest.co.in\\/public-api\\/products is returned filtered by categoryId") {
            exchange
                .expectStatus().isOk
                .expectBody().consumeWith { r ->
                    val rootNode = jacksonObjectMapper().readTree(ByteArrayInputStream(r.responseBody)).get("data") as ArrayNode
                    val categories: List<ArrayNode> = rootNode.toList().map{ it["categories"] as ArrayNode}
                    val all = categories.map{ category ->
                        category.toList().map{ it["id"].asLong() }
                    }.all{ c -> c.contains(categoryIdTest) }

                    assertTrue(all)
                }

        }
    }
}