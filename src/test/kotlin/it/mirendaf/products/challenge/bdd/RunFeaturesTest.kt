package it.mirendaf.products.challenge.bdd

import io.cucumber.junit.Cucumber
import io.cucumber.junit.CucumberOptions
import io.cucumber.spring.CucumberContextConfiguration
import org.junit.runner.RunWith
import org.springframework.boot.test.autoconfigure.webservices.client.AutoConfigureWebServiceClient
import org.springframework.boot.test.context.SpringBootTest

@RunWith(Cucumber::class)
@CucumberContextConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebServiceClient
@CucumberOptions(
        features= ["src/test/resources/cucumber/features"],
        glue = ["it.mirendaf.products.challenge.bdd"],
        tags = "not @ignored", plugin = ["pretty"],
)
class RunFeaturesTest {
}