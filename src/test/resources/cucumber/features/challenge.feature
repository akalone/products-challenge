Feature: Reading products from external resource

  Scenario: Reading all products retrieved from https://gorest.co.in/public-api/products
    Given a GET call to /products
    When no additional parameter is passed
    Then a list of products taken by https://gorest.co.in/public-api/products is returned

  Scenario: Reading products retrieved from https://gorest.co.in/public-api/products filtered by category
    Given a GET call to /products with no additional parameter
    When the categoryId Query Parameter is passed
    Then a list of products taken by https://gorest.co.in/public-api/products is returned filtered by categoryId