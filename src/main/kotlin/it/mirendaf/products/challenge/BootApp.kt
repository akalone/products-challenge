package it.mirendaf.products.challenge

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class BootApp

fun main(args: Array<String>) {
    runApplication<BootApp>(*args)
}