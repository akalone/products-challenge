package it.mirendaf.products.challenge.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.client.WebClient

@Configuration
class Config {
    @Bean("goRestWebClient")
    fun getWebClient(@Value("\${services.go-rest.base-url}") goRestProductsUrl: String): WebClient {
        return WebClient
            .builder()
            .baseUrl(goRestProductsUrl)
            .build();
    }
}