package it.mirendaf.products.challenge.utils

import org.springframework.web.reactive.function.client.ClientResponse
import reactor.core.publisher.Mono

typealias MapResponseFunction<T> = (ClientResponse) -> Mono<T>