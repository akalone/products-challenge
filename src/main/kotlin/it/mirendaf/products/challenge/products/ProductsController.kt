package it.mirendaf.products.challenge.products

import com.fasterxml.jackson.databind.JsonNode
import org.apache.logging.log4j.LogManager
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
class ProductsController (private val productsService: ProductsService) {
    private val logger = LogManager.getLogger(ProductsController::class.java)

    @GetMapping(value = ["/products"])
    fun getProducts(@RequestParam categoryId: Long?): Mono<JsonNode> {
        logger.info("Incoming Request on /products with categoryId: $categoryId")
        return productsService.getProducts(categoryId).map { x ->
            logger.info("Outgoing Response on /products: $x")
            x
        }
    }
}