package it.mirendaf.products.challenge.products

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.TextNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import it.mirendaf.products.challenge.extensions.Extensions.extract
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.ClientResponse
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

@Service
class ProductsService (val goRestProductsClient: GoRestProductsClient) {

    fun getProducts(categoryId: Long? = null): Mono<JsonNode> = goRestProductsClient.getProducts { r: ClientResponse ->
        val eventuallyFilteredBodyMono  = extractField<ArrayNode>(r, DataFieldName).map(ArrayNode::toList)

        eventuallyFilteredBodyMono.map{ filterByCategory(it, categoryId) }
            .flatMapMany{
                val categoriesNode = jacksonObjectMapper().createArrayNode().addAll(it)
                val rootNode = jacksonObjectMapper().createObjectNode().set<JsonNode>(DataFieldName, categoriesNode)
                Flux.just(rootNode)
            }.toMono()
    }

    companion object {
        private const val DataFieldName = "data"

        fun <T: JsonNode> extractField(response: ClientResponse, fieldName: String): Mono<T> {
            return response.bodyToMono(String::class.java).map { s ->
                val jsonNode = jacksonObjectMapper().readTree(s)
                jsonNode.extract<T>(fieldName)
            }
        }

        val filterByCategory = { data: List<JsonNode>, categoryId: Long? ->
            when (categoryId) {
                null -> data
                else -> data.filter { d ->
                    val categories = d["categories"] as ArrayNode
                    categories.map { it["id"].asLong() }.contains(categoryId)
                }
            }
        }

    }
}