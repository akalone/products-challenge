package it.mirendaf.products.challenge.products

import it.mirendaf.products.challenge.utils.MapResponseFunction
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.ClientResponse
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono

@Service
class GoRestProductsClient(@Qualifier("goRestWebClient") val webClient: WebClient) {

    fun <T : Any> getProducts(f: MapResponseFunction<T>): Mono<T> {
        return webClient.get().exchangeToMono(f)
    }

}