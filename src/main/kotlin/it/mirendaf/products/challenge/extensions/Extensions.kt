package it.mirendaf.products.challenge.extensions

import com.fasterxml.jackson.databind.JsonNode

object Extensions {
    fun <T: JsonNode> JsonNode.extract(field: String): T {
        return this.path(field) as T
    }

}