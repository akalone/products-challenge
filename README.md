# Products Challenge

Created with Spring Boot, Kotlin and Maven

## Useful commands:
If you don't have maven installed on your machine, you can use maven wrapper commands, changing *mvn* with *mvnw*

* Run the app (starts on localhost:8080)
  ```shell
  mvn clean spring-boot:run
  mvnw clean spring-boot:run
  ```
* Launch tests
  ```shell
  mvn clean test
  mvw clean test
  ```
* Build jar
  ```shell
  mvn clean install
  mvnw clean install
  ```